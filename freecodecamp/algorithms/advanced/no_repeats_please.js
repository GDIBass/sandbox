/**
 * Returns all permutations of a string - recursive
 *
 * @param   soFar   string    The string to check
 * @param   left    string    The characters left from the original string
 * @param   str     string    The full string
 * @returns array             The possible permutations
 */
function getAllPermutations(soFar, left, str) {
    // Define permutations, which will be our result
    let permutations = [];
    // If the string so far is the same as the full string length then we've hit the bottom of our recursion.
    if ( soFar.length === str.length ) {
        // Return just this value in an array
        permutations.push(soFar);
        return permutations;
    }

    // For the length of the remaining digits
    for ( let i = 0 ; i < left.length ; i++ ) {
        // Get all permutations for
        getAllPermutations(
            soFar + left[i],                    // What we have so far plus one of the remaining characters
            left.slice(0,i) + left.slice(i+1),  // The remaining characters with the added one removed
            str                                 // And the full string for comparison
        ).forEach(                              // Then we'll push each result to permutations
            (v) => permutations.push(v)
        );

    }
    return permutations;
}

/**
 * Returns count of permutations for a string with no repeating values
 *
 * @param   str  string     The string to check
 * @return  int             The number of permutations without repeats
 */
function permAlone(str) {
    // Get all permutations, starting with blank so far and the full strings
    return getAllPermutations('', str, str).filter(     // Filter the result
        (v) => v.split('').every(                       // Split each value into an array
            (val,ind,arr) => val !== arr[ind+1]         // And make sure every iteration of the digits in the array
        )                                               // ... don't match the next iteration
    ).length;                                           // And return the length of the resulting array
}

permAlone('aabb');
