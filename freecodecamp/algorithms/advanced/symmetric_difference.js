function sym() {

    function filterArray(array1, array2) {
        return array1.filter((v,i,a) => array2.indexOf(v) === -1);
    };

    var symDiff = function(accumulator, next) {
        return filterArray(accumulator,next).concat(filterArray(next,accumulator));
    };

    return [...arguments].reduce(symDiff).filter((v,i,a) => a.indexOf(v) === i).sort();
}

sym([1, 2, 3], [5, 2, 1, 4]);
