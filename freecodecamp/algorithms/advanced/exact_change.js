// Define the denominations
const Denominations = {
    'PENNY'       : 1,
    'NICKEL'      : 5,
    'DIME'        : 10,
    'QUARTER'     : 25,
    'ONE'         : 100,
    'FIVE'        : 500,
    'TEN'         : 1000,
    'TWENTY'      : 2000,
    'ONE HUNDRED' : 10000
};

function checkCashRegister(price, cash, cid) {
    // Getting weird issues with floats, converting to integers
    price = price  * 100;
    cash  = cash   * 100;

    // Reversing the array to start from the highest denom
    cid = cid.reverse();

    // Calculating Change Due and register total
    let changeDue = cash - price;
    let registerTotal = cid.reduce((accumulator,next) => accumulator + next[1]*100, 0);

    // If Register doesn't have enough, or if change due is exact we can return now
    if ( registerTotal < changeDue ) return "Insufficient Funds";
    else if ( registerTotal === changeDue ) return "Closed";

    // Otherwise we'll define a result array and proceed through
    var result = [];
    for ( i = 0 ; i < cid.length ; i++ ) {
        let intervalValue = Denominations[cid[i][0]];
        if ( cid[i][1] === 0 || intervalValue > changeDue ) {
            // No change to send if no money or if interval bigger than change, so continue through the loop
            continue;
        }

        // Get the total available for this interval, along with the required of this interval
        let intervalTotal = cid[i][1] * 100;
        let incrementRequired = Math.floor(changeDue / intervalValue) * intervalValue;

        // If the required is more than the total available use the total available
        let incrementApplied = incrementRequired > intervalTotal ? intervalTotal : incrementRequired;

        // Push the applied denoms to the resulting array
        result.push([cid[i][0], incrementApplied / 100]);
        changeDue -= incrementApplied;
    }

    // One last check to make sure we didn't end up with some change (Example: if there weren't enough pennies)
    if ( changeDue > 0 ) {
        return "Insufficient Funds";
    }
    // Return the result
    return result;
}

checkCashRegister(3.26, 100.00, [["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.10], ["QUARTER", 4.25], ["ONE", 90.00], ["FIVE", 55.00], ["TEN", 20.00], ["TWENTY", 60.00], ["ONE HUNDRED", 100.00]]);
