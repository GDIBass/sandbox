function orbitalPeriod(arr) {
    const GM = 398600.4418;
    const earthRadius = 6367.4447;
    return arr.map(function (v,i) {
        return {'name': v.name, 'orbitalPeriod': Math.round(2 * Math.PI * Math.pow(Math.pow(v['avgAlt'] + earthRadius, 3) / GM, 0.5))};
    });
}

orbitalPeriod([{name : "sputnik", avgAlt : 35873.5553}]);
