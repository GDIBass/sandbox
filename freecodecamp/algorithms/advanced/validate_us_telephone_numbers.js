function telephoneCheck(str) {
    // I hate regex
    if ( str.match(/\d+/g).length === 0 ) { // No numbers included
        return false;
    }

    if ( str[0] === '1' ) {
        str = str.slice(1).trim();
    }

    var numbersOnly = str.match(/\d+/g).join('');

    if ( numbersOnly.length !== 10 ) {
        return false;
    }

    if ( str[3] === ')' ) {
        return false;
    }

    if ( str[0] === '(' ) {
        if ( str[4] !== ')' ) {
            return false;
        } else {
            str = str.slice(1,3) + str.slice(5);
        }
    }

    if ( str.indexOf('(') !== -1 && str.indexOf(')') !== -1 ) {
        return false;
    }

    return true;
}



telephoneCheck("(555)5(55?)-5555");
