// Setup
var collection = {
    "2548": {
        "album": "Slippery When Wet",
        "artist": "Bon Jovi",
        "tracks": [
            "Let It Rock",
            "You Give Love a Bad Name"
        ]
    },
    "2468": {
        "album": "1999",
        "artist": "Prince",
        "tracks": [
            "1999",
            "Little Red Corvette"
        ]
    },
    "1245": {
        "artist": "Robert Palmer",
        "tracks": [ ]
    },
    "5439": {
        "album": "ABBA Gold"
    }
};
// Keep a copy of the collection for tests
var collectionCopy = JSON.parse(JSON.stringify(collection));

// Only change code below this line
function updateRecords(id, prop, value) {
    if ( collection[id] === undefined ) {
        return collection;
    }

    switch (prop) {
        case 'tracks':
            if ( collection[id][prop] === undefined ) {
                collection[id][prop] = [];
            }
            collection[id][prop].push(value);
            break;
        default:
            collection[id][prop] = value;
            break;
    }
    if ( value === '' ) {
        delete collection[id][prop];
    }

    return collection;
}

// Alter values below to test your code
updateRecords(2548, "tracks", "");

