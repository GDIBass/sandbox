function pairwise(arr, arg) {
    let resultingIndexes = [];
    arr.forEach(function (v,i,a) {
        arr.forEach(function(value,index,array) {
            if ( v + value === arg && i !== index ) {
                if ( resultingIndexes.indexOf(i) === -1 && resultingIndexes.indexOf(index) === -1 ) {
                    resultingIndexes.push(i);
                    resultingIndexes.push(index);
                }
            }
        });
    });
    return resultingIndexes.reduce((a,v) => a+v, 0);
}

pairwise([1, 1, 1], 2);
