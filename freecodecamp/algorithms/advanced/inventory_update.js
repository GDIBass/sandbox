function updateInventory(arr1, arr2) {
    // Define inventory object
    let inventory = {};
    // Store values in keys
    arr1.forEach((item) => inventory[item[1]] = item[0]);
    // Set value to added total or individual amount, depending on if we have inventory
    arr2.forEach((item) => inventory[item[1]] = inventory.hasOwnProperty(item[1]) ? inventory[item[1]] + item[0] : item[0] );
    // Convert back to array
    let result = [];
    Object.keys(inventory).forEach((prop) => result.push([inventory[prop], prop]));
    // Sort the array
    return result.sort((a,b) => a[1] > b[1]);
}

// Example inventory lists
updateInventory([[21, "Bowling Ball"], [2, "Dirty Sock"], [1, "Hair Pin"], [5, "Microphone"]], [[2, "Hair Pin"], [3, "Half-Eaten Apple"], [67, "Bowling Ball"], [7, "Toothpaste"]]);
