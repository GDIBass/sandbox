function destroyer(arr) {
    // Remove all the values
    var toRemove = Array.prototype.slice.call(arguments).slice(1);

    return arr.filter(function(val){
        return toRemove.indexOf(val) === -1;
    });
}

destroyer([1, 2, 3, 1, 2, 3], 2, 3);
