function rot13(str) {
    var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return str.split('').map(function(val){
        if ( ! val.match(/[A-Z]/) ) {
            return val;
        }
        var charIndex = alphabet.split('').indexOf(val);
        return alphabet[charIndex - 13 < 0 ? charIndex - 13 + 26 : charIndex - 13];
    }).join('');
}

rot13("SERR PBQR PNZC");
