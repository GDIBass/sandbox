function mutation(arr) {
    return arr[1].split('').map(function(char){
        return arr[0].toLowerCase().split('').indexOf(char.toLowerCase());
    }).indexOf(-1) === -1;
}

mutation(["Mary", "Army"]);
