function truncateString(str, num) {
    return str.length <= num ? str : num < 3 ? str.substr(0,num) + '...' : str.substr(0,num-3) + '...';
}

truncateString("A-tisket a-tasket A green and yellow basket", 20);
