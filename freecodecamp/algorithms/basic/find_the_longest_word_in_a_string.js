function findLongestWord(str) {
    var array = str.split(' ');
    var answer = '';
    for ( i = 0; i < array.length ; i ++ ) {
        if ( array[i].length > answer.length ) {
            answer = array[i];
        }
    }
    return answer.length;
}

findLongestWord("The quick brown fox jumped over the lazy dog");
