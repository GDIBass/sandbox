function palindrome(str) {
    str = str.toLowerCase().replace(/[^a-zA-Z0-9]+/g,'');
    // Good luck!
    return str === str.split('').reverse().join('');
}

palindrome("A man, a plan, a canal. Panama");
