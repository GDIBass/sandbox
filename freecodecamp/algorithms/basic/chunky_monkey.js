function chunkArrayInGroups(arr, size) {
    var result = [];
    for ( i = 0 ; i < arr.length ; i++ ) {
        var newKey = Math.floor(i/size);
        if ( result[newKey] === undefined ) {
            result[newKey] = [];
        }
        result[newKey].push(arr[i]);
    }
    return result;
}

chunkArrayInGroups(["a", "b", "c", "d"], 2);
