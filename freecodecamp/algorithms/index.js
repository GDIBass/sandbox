let runFunction = '';

/**
 * When clicking submit, call the function and display the output
 */
$(document).on('click', '#submit', function() {

    let output;
    if ( runFunction ) {
        try {
            let inputValue = $('#input').val();

            let funcToRun = runFunction + Date.now();

            let sendInputScript = document.createElement('script');
            sendInputScript.setAttribute('id', 'sendinputscript');
            sendInputScript.text = 'function ' + funcToRun + '() {'
                + '  return ' + runFunction + '(' + inputValue + ');'
                + '}';
            document.head.appendChild(sendInputScript);

            output = window[funcToRun]();
        } catch (e) {
            swal({
                'title' : 'Error',
                'type'  : 'error',
                'text'  : e
            });
            console.log(e);
        }
    } else {
        output = 'Pick a function first';
    }

    let jquerySelected = $('#select-script').find(':selected');

    if ( jquerySelected.data('display-html-entities') ) {
        output = output.replace(/&+/g,'&amp;');
    }
    $('#output').html(JSON.stringify(output, null, 2));
});

/**
 * Set the script input, reset the output, includes the javascript and displays the code
 */
function setScript() {
    // Get selected option
    let jquerySelected = $('#select-script').find(':selected');

    // Get the script
    let script = jquerySelected.data('src');
    // Remove existing
    $('#runscript').remove();

    // Add it to our headers and code
    let runScript = document.createElement('script');
    runScript.setAttribute('src', script);
    runScript.setAttribute('id', '#run-scropt');
    document.head.appendChild(runScript);

    // Add it to code display
    $('#code').html('');
    let xhr = new XMLHttpRequest;
    xhr.open('GET', script, true);
    xhr.onload = function () {
        $('#code').append(xhr.responseText);
    };
    xhr.send(null);

    // Set the function to run
    runFunction = jquerySelected.data('function');
    $('#funcName').html(runFunction);

    // Set the default input
    let defaultValue = jquerySelected.data('default');
    if ( jquerySelected.data('parse-default-as-json') ) {
        defaultValue = JSON.stringify(defaultValue);
    }
    $('#input').val(defaultValue);

    // Wipe output
    $('#output').html('');

    // Get Data Link
    var link = jquerySelected.data('link');
    var challengeLink = $('#challenge-link');
    challengeLink.attr('href', link);
    challengeLink.html(jquerySelected.html());
}