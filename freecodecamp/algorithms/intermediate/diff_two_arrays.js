function diffArray(arr1, arr2) {
    return arr1.filter(function(val){
        return arr2.indexOf(val) === -1;
    }).concat(
        arr2.filter(function(val){
            return arr1.indexOf(val) === -1;
        })
    );
}

diffArray(["diorite", "andesite", "grass", "dirt", "pink wool", "dead shrub"], ["diorite", "andesite", "grass", "dirt", "dead shrub"]);
