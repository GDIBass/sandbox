/**
 * Returns true or false for solution options
 *
 * @param   solutionValue   int   An integer to test
 * @param   arr             array The array to test
 * @return  bool            Whether or not the solution is valid
 */
function isSolution (solutionValue, arr) {
    arr.sort();
    for ( j = arr[0] ; j <= arr[1] ; j++ ) {
        if ( solutionValue % j !== 0 ) {
            return false;
        }
    }
    return true;
}

function smallestCommons(arr) {
    // Declare the value outside of loops
    var solutionValue;

    // Sort the array to go from lowest to highest
    arr.sort();

    // The solution will be a multiple, so we'll just try multiplying tha multiple by incrementing values
    var i = 0;

    // Do while, so we can evaluate the solution at the end
    do {
        // Increment by one so we start off not dividing by 0
        i++;

        // The solution value will be this
        solutionValue = i * arr[0] * arr[1];

        // Check if it's a solution
    }while ( isSolution(solutionValue, arr) === false );
    return solutionValue;
}


smallestCommons([5,1]);
