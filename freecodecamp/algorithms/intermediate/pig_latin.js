function translatePigLatin(str) {
    var vowels = ['a','e','i','o','u'];
    for ( i = 0 ; i < str.length ; i++ ) {
        if ( vowels.indexOf(str[i]) !== -1 ) {
            if ( i === 0 ) {
                return str + 'way';
            } else {
                return str.substr(i) + str.substr(0,i) + 'ay';
            }
        }
    }
    return str;
}

translatePigLatin("california");
