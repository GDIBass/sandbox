function convertHTML(str) {
    let encoder = {
        '&' : '&amp;',
        '<' : '&lt;',
        '>' : '&gt;',
        '"' : '&quot;',
        "'" : '&apos;'
    };
    var result = str.split('').map(function(val){
        console.log(val);
        console.log(encoder.hasOwnProperty(val));
        console.log(encoder[val]);
        return encoder.hasOwnProperty(val) ? encoder[val] : val;
    }).join('');
    console.log(result);
    return result;
}

convertHTML("Hamburgers < Pizza < Tacos");
