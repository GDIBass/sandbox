function addTogether() {
    let args = Array.from(arguments);
    return args.every(a => typeof a === 'number') ?
        args.length === 1 ?
            value => {return typeof value === 'number' ? value + args[0] : undefined;}
            :
            args.reduce(function(a,b){return a+b;})
        :
        undefined
        ;
}

addTogether(2)(3);
