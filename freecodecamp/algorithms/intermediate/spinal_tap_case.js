function spinalCase(str) {
    // "It's such a fine line between stupid, and clever."
    // --David St. Hubbins
    var spaced = true;
    return str.split('').map(function(val){
        if ( val === ' ' || val === '_' || val === '-' ) {
            spaced = true;
            return '-';
        }

        if ( val.toUpperCase() === val && spaced === false) {
            spaced = false;
            return '-' + val;
        }
        spaced = false;
        return val;
    }).join('').toLowerCase();
}

spinalCase('This Is Spinal Tap');
