function myReplace(str, before, after) {
    return str.split(' ').map(function(val) {
        if ( val !== before ) {
            return val;
        }
        if ( before[0].toUpperCase() === before[0] ) {
            return after[0].toUpperCase() + after.substr(1);
        }
        return after;
    }).join(' ');
}

myReplace("He is Sleeping on the couch", "Sleeping", "sitting");
