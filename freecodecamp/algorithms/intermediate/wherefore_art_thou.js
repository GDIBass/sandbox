function whatIsInAName(collection, source) {
    // What's in a name?
    var arr = [];
    // Only change code below this line
    // For each object in the arry
    for ( i = 0 ; i < collection.length ; i ++ ) {
        // Store it to an easy to reference variable
        thisObject = collection[i];
        // We'll use a boolean to determine if the object can be passed to the resulting array
        var include = true;
        // For each property in our source
        for ( var property in source ) {
            // Check to see if the property equals the array value in the collection
            if ( thisObject[property] !== source[property] ) {
                // If it doesn't it can't be included
                var include = false;
            }
        }
        // Push it to the result array if it can be included
        if ( include === true ) {
            arr.push(thisObject);
        }
    }
    // Only change code above this line
    return arr;
}

whatIsInAName([{ "a": 1, "b": 2 }, { "a": 1 }, { "a": 1, "b": 2, "c": 2 }], { "a": 1, "b": 2 });
