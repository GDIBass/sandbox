function sumPrimes(num) {
    primes = [];
    for ( i = 2 ; i <= num ; i++ ) {
        var isPrime = true;
        for ( j = 0 ; j < primes.length ; j++ ) {
            if ( i % primes[j] === 0 ) {
                isPrime = false;
            }
        }
        if ( isPrime ) {
            primes.push(i);
        }
    }
    return primes.reduce(function(prev,curr) {return prev+curr;});
}

sumPrimes(10);
