/**
 * Function used to convert a number to Roman Numerals
 * @return  string              Roman Numerals
 * @param   num       int       The number to convert, < 9000
 */
function convertToRoman(num) {
    /* Declare numerals */
    var numerals = [
        {
            1 : 'I',
            5 : 'V'
        }, {
            1 : 'X',
            5 : 'L'
        }, {
            1: 'C',
            5: 'D'
        }, {
            1: 'M'
        }
    ];
    // Declare the result
    var result = [];
    // Convert the number to a string, split it, then convert the results back to numbers and reverse (we want to start from the bottom)
    var numSplit = num.toString(10).split('').map(function(val){return parseInt(val);}).reverse();

    for ( power = 0 ; power < numSplit.length ; power ++ ) {
        // Get our current power
        var currentPower = numSplit[power];

        // Add to the results array depending on what our current power is
        if ( currentPower === 9 ) {
            result.push(numerals[power][1] + numerals[power+1][1]);
        } else if ( currentPower >= 5 ) {
            result.push(numerals[power][5] + numerals[power][1].repeat(currentPower - 5));
        } else if ( currentPower == 4 ) {
            result.push(numerals[power][1] + numerals[power][5] );
        } else {
            result.push(numerals[power][1].repeat(currentPower));
        }
    }
    // Reverse the result array and join it together
    return result.reverse().join('');
}

convertToRoman(2019);
