function sumAll(arr) {
    arr = arr.sort(function(a, b) {return a > b;});
    return arr.reduce(function(previousVal, currentVal) {
        var result = 0;
        for ( i = previousVal ; i <= currentVal; i++ ) {
            result += i;
        }
        return result;
    });
}

sumAll([4, 1]);
