var alphabet="abcdefghijklmnopqrstuvwxyz";
function fearNotLetter(str) {
    var start = alphabet.split('').indexOf(str[0]);
    for ( i = 0 ; i < str.length ; i++ ) {
        if ( alphabet[start+i] !== str[i] ) {
            return alphabet[start + i];
        }
    }
}

fearNotLetter("abce");
