function uniteUnique(arr) {
    var result = [];
    for ( i = 0 ; i < arguments.length ; i++ ) {
        for ( j = 0 ; j < arguments[i].length ; j++ ) {
            if ( result.indexOf(arguments[i][j]) === -1 ) {
                result.push(arguments[i][j]);
            }
        }
    }
    return result;
}

uniteUnique([1, 3, 2], [5, 2, 1, 4], [2, 1]);
