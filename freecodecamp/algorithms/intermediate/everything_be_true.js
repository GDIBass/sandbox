function truthCheck(collection, pre) {
    return collection.every((coll) => coll[pre]);
}

truthCheck([{"user": "Dipsy"}, {"user": "Tinky-Winky", "sex": "male"}, {"user": "Laa-Laa", "sex": "female"}, {"user": "Po", "sex": "female"}], "sex");
