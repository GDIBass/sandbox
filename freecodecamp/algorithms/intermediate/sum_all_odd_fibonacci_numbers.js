function sumFibs(num) {
    total   = 0;
    prev    = 0;
    current = 1;
    do {
        if ( current % 2 === 1 ) {
            total += current;
        }
        val = prev + current;
        prev = current;
        current = val;
    } while( current <= num );
    return total;
}

sumFibs(4);
