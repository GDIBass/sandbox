function steamrollArray(arr) {
    var flattened = [].concat(...arr);
    return flattened.some(Array.isArray) ? steamrollArray(flattened) : flattened;
}

steamrollArray([1, [2], [3, [[4]]]]);
