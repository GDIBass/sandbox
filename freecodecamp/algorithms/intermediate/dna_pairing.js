var pairing = {
    'A' : 'T',
    'T' : 'A',
    'G' : 'C',
    'C' : 'G'
};
function pairElement(str) {
    return str.split('').map(function(val) {
        return [val,pairing[val]];
    });
}

pairElement("GCG");
