/**
 * Open the modal when pay now is clicked
 */
$(document).on('click', '.pay-now', function(event) {
    event.preventDefault();
    $('.modal').modal({
        backdrop: 'static',
        keyboard: false
    });
    let payAmount = $('#pay-amount').val().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    $('.pay-amount').html(payAmount);
});

/**
 * Updates the pay button to be enabled/disabled
 *
 * @param enabled
 */
function allowPayButton(enabled) {
    let payButton = $('.btn-pay');
    payButton.removeClass('btn-dark btn-success');
    if ( enabled ) {
        payButton.addClass('btn-success');
        payButton.prop('disabled', false);
    } else {
        payButton.addClass('btn-dark');
        payButton.prop('disabled', true);
    }
}

/**
 * Checks to see if form is completed and enables the pay button if it is
 * Disables it if it's not
 */
function checkComplete()
{
    // If no name is set
    if ( $('#credit-card-name').val() === '' ) {
        allowPayButton(false);
        return;
    }

    // If the credit card is not set or is invalid
    let creditCardNumber = $('#credit-card-number').val();
    if ( creditCardNumber === '' || creditCardNumber === undefined ) {
        allowPayButton(false);
        return;
    }

    let creditCardType = getCardType(creditCardNumber);
    if ( creditCardType === null ) {
        // No number
        allowPayButton(false);
        return;
    } else if ( creditCardType === 'AmericanExpress' && creditCardNumber.length !== 15 ) {
        // Amex, but wrong length
        allowPayButton(false);
        return;
    } else if ( creditCardNumber.length !== 16 ) {
        // Wrong Length
        allowPayButton(false);
        return;
    }

    // Check the CCV
    let ccv = $('#credit-card-ccv').val();
    if ( ccv.length !== 3 ) {
        allowPayButton(false);
        return;
    }

    // Check Expiry
    let ccExpiresMonth = $('#credit-card-expires-month').val();
    if ( ccExpiresMonth === null ) {
        allowPayButton(false);
        return;
    }

    let ccExpiresYear = $('#credit-card-expires-year').val();
    if ( ccExpiresYear === null ) {
        allowPayButton(false);
        return;
    }

    // If we go there then we can pay!
    allowPayButton(true);
}

/**
 * Set the name on the card display to this name
 */
$(document).on('keyup','#credit-card-name', function() {
    let creditCardName = $('.credit-card-name');
    let ccNameValue = $(this).val().length === 0 ? 'John Doe' : $(this).val();
    creditCardName.html(ccNameValue);
    checkComplete();
});

/**
 * If the name is too long.  We could still get some formatting issues, but that's acceptable
 */
$(document).on('keypress', '#credit-card-name', function(event) {
    if ( window.getSelection().toString() !== '' ) {
        return;
    }

    let currentName = $(this).val();
    if ( currentName.length >= 30 ) {
        event.preventDefault();
    }
});

/**
 * Get the card type based ont he number
 *
 * @param cardNumber
 * @returns string|null
 */
function getCardType(cardNumber) {
    const visa          = new RegExp(/^4/);
    const masterCard    = new RegExp(/^[5][1-5]/);
    const amex          = new RegExp(/^(34|37)/);

    if ( cardNumber === '' || cardNumber === undefined ) {
        return null;
    } else if ( visa.test(cardNumber) ) {
        return 'Visa';
    } else if ( masterCard.test(cardNumber) ) {
        return 'MasterCard';
    } else if ( amex.test(cardNumber) ) {
        return 'AmericanExpress';
    }
}

/**
 * Prevent non numeric ([eE.] inputs)
 */
$(document).on('keypress', '#credit-card-number,#credit-card-ccv', function(event) {
    if ( window.getSelection().toString() !== '' ) {
        return;
    }

    if ( ( event.which < 48 || event.which > 57) && event.which !== 8 && event.which !== 46 ) {
        event.preventDefault();
    }
});

/**
 * Prevent keypress if on max length
 */
$(document).on('keypress', '#credit-card-number', function(event){
    if ( window.getSelection().toString() !== '' ) {
        return;
    }

    let currentValue = $(this).val();
    let cardType = getCardType(currentValue);
    let maxLength = 16;

    if ( cardType === 'AmericanExpress' ) {
        maxLength = 15;
    }

    if ( currentValue.length === maxLength ) {
        event.preventDefault();
    }
});

/**
 * On Keyup set the CC on the display
 */
$(document).on('keyup','#credit-card-number', function() {
    let cardNumber = $(this).val();

    let cardDisplay = {
        0: 'X',
        1: 'X',
        2: 'X',
        3: 'X',
        4: ' ',
        5: 'X',
        6: 'X',
        7: 'X',
        8: 'X',
        9: ' ',
        10: 'X',
        11: 'X',
        12: 'X',
        13: 'X',
        14: ' ',
        15: 'X',
        16: 'X',
        17: 'X',
        18: 'X'
    };



    let creditCardImage = $('.credit-card-image');

    let creditCardType = $('.credit-card-type');

    let cardType = getCardType(cardNumber);

    creditCardImage.removeClass('cc-visa cc-mastercard cc-amex');

    if ( cardType === null ) {
        creditCardType.html('Credit Card');
    } else if ( cardType === 'Visa' ) {
        creditCardImage.addClass('cc-visa');
        creditCardType.html('Visa');
    } else if ( cardType === 'MasterCard' ) {
        creditCardImage.addClass('cc-mastercard');
        creditCardType.html('Master Card');
    } else if ( cardType === 'AmericanExpress' ) {
        creditCardImage.addClass('cc-amex');
        creditCardType.html('American Express');
        delete cardDisplay[18];
    }

    for ( let i = 0 ; i < cardNumber.length ; i++ ) {
        let index = i;
        if ( i > 3 ) {
            index++;
        }
        if ( i > 7 ) {
            index++;
        }
        if ( i > 11 ) {
            index++;
        }
        if ( cardDisplay.hasOwnProperty(index) ) {
            cardDisplay[index] = cardNumber[i];
        }
    }

    $('.credit-card-number').html(Object.values(cardDisplay).reduce((a,b) => a + b));
    checkComplete();
});

/**
 * Validate the CC on Blur
 */
$(document).on('blur', '#credit-card-number', function() {
    let cardNumber = $(this).val();
    if ( cardNumber.length === 0 ) {
        return;
    }
    let cardType = getCardType(cardNumber);
    let cardLength = 16;
    if ( cardType === 'AmericanExpress' ) {
        cardLength = 15;
    }

    if ( cardNumber.length !== cardLength || cardType === null || cardNumber.match(/[^$,.\d]/) || cardNumber.match(/e/) ) {
        swal({
            'title' : 'Error',
            'type'  : 'error',
            'text'  : 'Please enter a valid card number.  We only accept Visa, Mastercard and American Express.'
        }).then(function() {
            $('#credit-card-number').focus();
        });
    }
});

/**
 * Update the card expiry on change
 */
$(document).on('change','#credit-card-expires-month', function() {
    $('.cc-month').html($(this).val());
    checkComplete();
});

/**
 * Update the card expiry on change
 */
$(document).on('change','#credit-card-expires-year', function() {
    $('.cc-year').html($(this).val());
    checkComplete();
});

/**
 * Prevent keypress if already at max
 */
$(document).on('keypress','#credit-card-ccv', function(event) {
    if ( window.getSelection().toString() !== '' ) {
        return;
    }

    if ( $(this).val().length === 3 ) {
        event.preventDefault();
    }
});

/**
 * Update CCV on card when keyup on ccv form
 */
$(document).on('keyup', '#credit-card-ccv', function() {
    let ccv = $(this).val();
    let ccvDisplay = {
        0 : 'X',
        1 : 'X',
        2 : 'X',
    };
    for ( let i = 0 ; i < ccv.length ; i ++ ) {
        if ( ccvDisplay.hasOwnProperty(i) ) {
            ccvDisplay[i] = ccv[i];
        }
    }
    $('.ccv-on-card').html(Object.values(ccvDisplay).reduce((a,b) => a + b));
    checkComplete();
});

/**
 * Flip the card when ccv is focused
 */
$(document).on('focus', '#credit-card-ccv', function() {
    $('.full-credit-card').addClass('flipped');
});

/**
 * And flip it back when ccv is blurred
 */
$(document).on('blur', '#credit-card-ccv', function() {
    $('.full-credit-card').removeClass('flipped');
});

$(document).on('click', '.btn-pay', function() {
    if ( $(this).prop('disabled') ) {
        return;
    }
    swal({
        title: 'Submitting Payment',
        showConfirmButton: false,
        html: '<div style="height: 200px; line-height:250px;"><i class="fa fa-5 fa-cog fa-spin animated" style="font-size:100px;"></i></div>',
        allowOutsideClick: false,
        allowEscapeKey: false
    });

    setTimeout(function() {
        swal({
            title: 'Success!',
            type: 'success',
            html: 'Your payment has been processed!'
        }, function() {
        }).then(function() {
            $('#credit-card-name').val('');
            $('#credit-card-number').val('');
            $('#credit-card-ccv').val('');
            $('#credit-card-expires-month').val('None');
            $('#credit-card-expires-year').val('None');
            $('.credit-card-type').html('Credit Card');
            $('.credit-card-number').html('XXXX XXXX XXXX XXXX');
            $('.credit-card-name').html('John Doe');
            $('.cc-month').html('XX');
            $('.cc-year').html('XX');
            $('.ccv-on-card').html('XXX');
            $('.credit-card-image').removeClass('cc-visa cc-mastercard cc-amex');
            checkComplete();
            $('.modal').modal('hide');
        });
    },3000);
});