<?php
/**
 * Created by PhpStorm.
 * User: matthewjohnson
 * Date: 4/6/18
 * Time: 10:35 AM
 */

/**
 * Pass in an array of arrays to check against eachother
 *
 * @param array $array  An array of arrays to check
 * @return array
 */
function check_all_arrays_for_matches($array)
{
    // Define our results array
    $result = [];

    // For each array item
    foreach ( $array as $key => $subArray ) {
        // Test array is the same array, minus the one we're looking at
        $testArray = $array;
        unset($testArray[$key]);

        // Map the array and add to results
        $result[$key] = array_map(
        // For the map we define sub test (the individual Array from test array to test)
        // We also pass in the sub array, unmodified to test against
            function($subTestArray) use ($subArray) {
                // We filter the static sub array against the individual sub test passed in
                return array_filter(
                    // Pass in the array to filter
                    $subArray,
                    // And a function which uses that array's values ($arrayValue) and tests ita gainst a static $subTestArray
                    function($arrayValue) use ($subTestArray) {
                        // Return whether or not the value is in the test array
                        return in_array($arrayValue, $subTestArray);
                    }
                );

            },
            // Array map takes in an array to test
            $testArray
        );
    }
    return $result;
}

$input = [
    'bandwidth'     => [
        '18582222222',
        '18583333333',
        '18584444444'
    ],
    'iq'            => [
        '18584444444',
        '18585555555',
        '18586666666',
        '18587777777'
    ],
    'level3'        => [
        '18582222222',
        '18585555555',
        '18588888888',
        '18589999999'
    ],
    'othervendor'   => [
        '18582222222',
        '18584444444',
        '18588888888',
        '17602222222'
    ],
    'othervendor2'  => [
        '17603333333',
        '17604444444',
        '17605555555',
        '17606666666'
    ]
];

print_r(check_all_arrays_for_matches($input));
